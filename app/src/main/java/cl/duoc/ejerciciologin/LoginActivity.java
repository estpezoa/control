package cl.duoc.ejerciciologin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.ejerciciologin.entidades.Formulario;

public class LoginActivity extends AppCompatActivity {

    private Button btnAceptar, btnRegistro;
    private EditText etContrasena, etUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnAceptar = (Button) findViewById(R.id.btnAceptar);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);
        etContrasena = (EditText) findViewById(R.id.etContrasena);
        etUsuario = (EditText) findViewById(R.id.etUser);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etContrasena.getText().toString().trim().equals("") || etUsuario.getText().toString().trim().equals("")) {
                    Toast.makeText(LoginActivity.this, "Por favor complete sus datos", Toast.LENGTH_SHORT).show();
                }else{
                boolean existe=false;

                if (RegistroActivity.formularios.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "No hay usuarios creados", Toast.LENGTH_SHORT).show();
                } else {

                    for (Formulario f : RegistroActivity.formularios) {
                        if (etUsuario.getText().toString().equals(f.getUsuario()) && etContrasena.getText().toString().equals(f.getContrasena())) {
                           existe=true;
                        }
                    }
                    if(existe) Toast.makeText(LoginActivity.this, "Logeado", Toast.LENGTH_SHORT).show();
                    else  Toast.makeText(LoginActivity.this, "No existe usuario", Toast.LENGTH_SHORT).show();
                }}



            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });

    }
}
