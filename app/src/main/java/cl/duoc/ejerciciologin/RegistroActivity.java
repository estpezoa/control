package cl.duoc.ejerciciologin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cl.duoc.ejerciciologin.entidades.Formulario;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAceptarRegistro, btnVolver;
    private EditText etUsuario, etNombres, etApellidos, etRut, etFechaNacimiento, etContrasena, etRepetirContrasena;
    public static ArrayList<Formulario> formularios = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        btnAceptarRegistro = (Button) findViewById(R.id.btnAceptarRegistro);
        btnVolver = (Button) findViewById(R.id.btnVolver);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etNombres = (EditText) findViewById(R.id.etNombres);
        etApellidos = (EditText) findViewById(R.id.etApellidos);
        etRut = (EditText) findViewById(R.id.etRut);
        etFechaNacimiento = (EditText) findViewById(R.id.etFechaNacimiento);
        etContrasena = (EditText) findViewById(R.id.etContrasena);
        etRepetirContrasena = (EditText) findViewById(R.id.etRepetirContrasena);

        btnVolver.setOnClickListener(this);
        btnAceptarRegistro.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnVolver) {
            Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
            startActivity(i);
        } else if (v.getId() == R.id.btnAceptarRegistro) {

            if (etNombres.getText().toString().trim().equals("") ||
                    etNombres.getText().toString().trim().equals("") ||
                    etApellidos.getText().toString().trim().equals("") ||
                    etContrasena.getText().toString().trim().equals("") ||
                    etRepetirContrasena.getText().toString().trim().equals("") ||
                    etRut.getText().toString().trim().equals("") ||
                    etFechaNacimiento.getText().toString().trim().equals("") ||
                    etUsuario.getText().toString().trim().equals("")) {
                Toast.makeText(this, "Por favor complete sus datos", Toast.LENGTH_SHORT).show();
            } else {
                if (etContrasena.getText().toString().equals(etRepetirContrasena.getText().toString())) {
                    Formulario f = new Formulario();
                    f.setNombres(etNombres.getText().toString());
                    f.setApellidos(etApellidos.getText().toString());
                    f.setContrasena(etContrasena.getText().toString());
                    f.setRut(etRut.getText().toString());
                    f.setFechaNacimiento(etFechaNacimiento.getText().toString());
                    f.setUsuario(etUsuario.getText().toString());
                    formularios.add(f);
                    Toast.makeText(this, "Usuario " + f.getUsuario() + " creado", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
                    startActivity(i);


                } else {
                    Toast.makeText(this, "Contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }
}

